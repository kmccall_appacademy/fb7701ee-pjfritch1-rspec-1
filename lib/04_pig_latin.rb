def translate(words)

  latin_words = []
  split_words = []

  normie_words = words.split(" ")

  normie_words.each {|word| split_words << word.split("")}
  split_words.each do |wrd|
    wrd.each_with_index do |letter, idx|
      if "aeiou".include?(letter)
        if letter == "u" && wrd[idx-1] == "q"
          idx += 1
        end
        latin_suffix = wrd.slice!(0...idx)
        wrd << latin_suffix
        latin_words << "#{wrd.join}ay"
        break
      end
    end
  end

  latin_words.join(" ")

end
