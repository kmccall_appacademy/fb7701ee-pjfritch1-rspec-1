def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, num_times = 2)
  repeated = []
  num_times.times {repeated << string}
  repeated.join(" ")
end

def start_of_word(word, number_of_letters)
  word[0...number_of_letters]
end

def first_word(string)
  words = string.split(" ")
  words[0]
end

def titleize(string)
  sentence = string.split(" ")
  little_words = ["the","of","and","a","an","at","over"]
  sentence[0].capitalize!
  sentence.each do |word|
    word.capitalize! unless little_words.include?(word)
  end
  titleized = sentence.join(" ")
end
