def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(nums)
  sum = 0
  nums.each {|num| sum += num}
  sum
end

def multiply(nums)
  result = 1
  nums.each {|num| result *= num}
  result
end

def power(num1, num2)
  num1 ** num2
end

def factorial(num)
  result = 1
  (1..num).each {|i| result *= i} if num > 0
  result
end
